import os
from typing import Dict

import tensorflow as tf
import tensorflow_datasets as tfds

from tensorflow_sandbox.config import Config


def build_dataset(dataset_dir: str, config: Config, experiment: bool) \
        -> (tf.data.Dataset, tf.data.Dataset, tfds.core.DatasetInfo):
    """Will load imagenet2012 and construct a training and validation set

    Note--this will download imagenet2012 if it can't find it within dataset_dir! Total dataset is ~150GB.

    Args:
        dataset_dir: Path to tfds directory containing the dataset
        config: A config to build the pipeline with
        experiment: Whether to load the experiment dataset or the full dataset

    Returns:
        A dataset where each item is (features, labels), where features is a dict with keys ('image') and
            labels is a dict with keys ('label') split into 90% training and 10% validation

    """
    dataset_dir = os.path.join(dataset_dir, 'experiments') if experiment else dataset_dir
    train_dataset, dataset_info = tfds.load('imagenet2012', data_dir=dataset_dir, split='train', with_info=True,
                                            download=True)
    train_dataset = train_dataset.map(lambda features: preprocess_imagenet(features, config))
    train_dataset = train_dataset.shuffle(config.random_seed).batch(config.batch_size, drop_remainder=True).prefetch(10)

    val_dataset = tfds.load('imagenet2012', data_dir=dataset_dir, split='validation')
    val_dataset = val_dataset.map(lambda features: preprocess_imagenet(features, config))
    val_dataset = val_dataset.shuffle(config.random_seed).batch(config.batch_size).prefetch(10)

    return train_dataset, val_dataset, dataset_info


def build_inference_testset(dataset_dir: str, config: Config):
    val_dataset = tfds.load('imagenet2012', data_dir=dataset_dir, split='validation')
    val_dataset = val_dataset.map(lambda features: preprocess_imagenet(features, config, inference=True))
    val_dataset = val_dataset.batch(config.batch_size).prefetch(10)
    return val_dataset


def preprocess_imagenet(features: Dict[str, tf.Tensor], config: Config, inference: bool = False) \
        -> (Dict[str, tf.Tensor], Dict[str, tf.Tensor]):
    """Apply transformations to resnet2012 dataset

    Args:
        features: Dictionary with keys ('image', 'label', 'filename')
        config: Which config to use
        inference: Whether to preprocess for inference or not

    """
    resized_image = tf.image.resize_with_crop_or_pad(features['image'], target_height=config.image_height,
                                                     target_width=config.image_width)
    if not inference:
        cast_image = tf.cast(resized_image, dtype=tf.float32)
        mean_normalized_image = cast_image - config.mean_pixel
        normalized_image = tf.divide(mean_normalized_image, config.sdev_pixel)

    else:
        normalized_image = tf.cast(resized_image, dtype=tf.uint8)
    labels = {'label': features['label']}
    return {'image': normalized_image, 'og': resized_image}, labels

