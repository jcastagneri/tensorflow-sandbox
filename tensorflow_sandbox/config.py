from typing import List


class Config:
    def __init__(self, batch_size: int = 32, learning_rate: float = 0.1, momentum: float = 0.9,
                 weight_decay: float = 0.000, image_height: int = 224, image_width: int = 224,
                 num_layers: int = 34, mean_pixel: List[float] = None, random_seed: int = 42,
                 log_steps: int = 200, summary_frequency: int = 500, validation_steps: int = 350,
                 max_checkpoints: int = 5, sdev_pixel: List[float] = None, train_decoder: bool = True):
        if mean_pixel is None:
            mean_pixel = [118.76671, 110.69113, 96.45964]
        if sdev_pixel is None:
            sdev_pixel = [50.26406, 49.21388, 48.435608]

        # Training params
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.weight_decay = weight_decay
        self.train_decoder = train_decoder

        # Architecture params
        self.image_height = image_height
        self.image_width = image_width
        self.num_layers = num_layers

        # Augmentation Params
        self.mean_pixel = mean_pixel  # Computed over 32000 randomly sampled images
        self.sdev_pixel = sdev_pixel  # Computed over 32000 randomly sampled images

        # Runner params
        self.random_seed = random_seed
        self.log_steps = log_steps
        self.summary_frequency = summary_frequency
        self.validation_steps = validation_steps
        self.max_checkpoints = max_checkpoints


class FullTrainConfig(Config):
    def __init__(self, epochs: int = 100, validation_frequency: int = 2000, checkpoint_frequency: int = 15000,
                 *args, **kwargs):
        super(FullTrainConfig, self).__init__(*args, **kwargs)
        # Runner params
        self.epochs = epochs
        self.validation_frequency = validation_frequency
        self.checkpoint_frequency = checkpoint_frequency


class ExperimentConfig(Config):
    def __init__(self, epochs: int = 5, validation_frequency: int = 500, checkpoint_frequency: int = 5000,
                 *args, **kwargs):
        super(ExperimentConfig, self).__init__(*args, **kwargs, summary_frequency=100)
        # Runner params
        self.epochs = epochs
        self.validation_frequency = validation_frequency
        self.checkpoint_frequency = checkpoint_frequency


def config_factory(experiment: bool):
    if experiment:
        return ExperimentConfig()
    else:
        return FullTrainConfig()

