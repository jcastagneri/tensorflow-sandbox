import logging
import sys

import click
from colorlog import ColoredFormatter

from tensorflow_sandbox.bin.compute_normalization import compute_normalization_cli
from tensorflow_sandbox.bin.compute_normalization import histogram_pixels_cli
from tensorflow_sandbox.bin.summarize_model import summarize_cli
from tensorflow_sandbox.bin.train_model import train_model_cli
from tensorflow_sandbox.bin.test_model import test_cli
from tensorflow_sandbox.bin.visualize_imagenet import visualize_imagenet_cli

formatter = ColoredFormatter('%(log_color)s%(asctime)-2s| %(levelname)-9s| %(name)-55s | %(message)s',
                             '%Y-%m-%d %H:%M:%S')
handler = logging.StreamHandler()
handler.setFormatter(formatter)
logging.basicConfig(level=logging.INFO, handlers=[handler], format=formatter)
logger = logging.getLogger('tensorflow_sandbox.bin')


@click.group('sandbox-tools')
def sandbox_tools_cli():
    pass


sandbox_tools_cli.add_command(train_model_cli)
sandbox_tools_cli.add_command(visualize_imagenet_cli)
sandbox_tools_cli.add_command(test_cli)
sandbox_tools_cli.add_command(summarize_cli)
sandbox_tools_cli.add_command(compute_normalization_cli)
sandbox_tools_cli.add_command(histogram_pixels_cli)


if __name__ == '__main__':
    sandbox_tools_cli(sys.argv[1:])
