import logging
import os
import json
from collections import namedtuple
from importlib import resources

import click
import numpy as np
import tensorflow as tf
from tensorflow import keras

from tensorflow_sandbox.data_pipeline.load_imagenet import build_dataset
from tensorflow_sandbox.config import config_factory
from tensorflow_sandbox.models.autoencoder import AutoEncoder
from tensorflow_sandbox.utils.metrics import MetricContainer
from tensorflow_sandbox.utils.tools import build_model

logger = logging.getLogger(__name__)

LossContainer = namedtuple('LossContainer', ['class_loss', 'autoencoder_loss'])


@click.command('train-model', help='Script to train a resnet model.')
@click.argument('log-dir', type=click.Path())
@click.argument('model-name', type=str)
@click.option('--data-dir', help='Containing directory of TF datasets (assumes imagenet2012)',
              type=click.Path(exists=True), default=os.path.join(os.path.expanduser('~'), 'data',
                                                                 'tensorflow-datasets'))
@click.option('--experiment/--noexperiment', help='Whether or not this is an experiment')
def train_model_cli(data_dir: str, log_dir: str, model_name: str, experiment: bool):
    os.makedirs(log_dir, exist_ok=True)
    os.makedirs(os.path.join(log_dir, 'exports'), exist_ok=True)

    config = config_factory(experiment)

    logger.debug('Creating input pipeline...')
    train_dataset, val_dataset, dataset_info = build_dataset(data_dir, config, experiment)
    steps_per_epoch = int(np.floor(dataset_info.splits['train'].num_examples / config.batch_size))

    logger.debug('Creating model and training objects...')
    model = build_model(model_name, config)
    loss_object = LossContainer(
        class_loss=keras.losses.SparseCategoricalCrossentropy(),
        autoencoder_loss=keras.losses.Huber()
    )
    optimizer = keras.optimizers.SGD(learning_rate=config.learning_rate, momentum=config.momentum)
    model, current_epoch, current_step_in_epoch = load_checkpoint_if_relevant(model, log_dir, val_dataset, loss_object,
                                                                              optimizer, steps_per_epoch,
                                                                              config.max_checkpoints,
                                                                              config.train_decoder)

    logger.debug('Setting up metrics...')
    train_metrics = MetricContainer(num_categories=1000,
                                    loss_metrics=[keras.metrics.Mean(name='categorical_loss'),
                                                  keras.metrics.Mean(name='similarity_loss')],
                                    sparse_prediction_metrics=[
                                        keras.metrics.SparseCategoricalAccuracy(name='accuracy'),
                                        keras.metrics.SparseTopKCategoricalAccuracy(name='top_5_accuracy', k=5),
                                    ],
                                    dense_prediction_metrics=[
                                        keras.metrics.AUC(name='auc', num_thresholds=200, curve='PR'),
                                        keras.metrics.Precision(name='precision_0.5', top_k=1, thresholds=0.5),
                                        keras.metrics.Recall(name='recall_0.5', top_k=1, thresholds=0.5)
                                    ])
    val_metrics = MetricContainer(num_categories=1000,
                                  loss_metrics=[keras.metrics.Mean(name='categorical_loss'),
                                                keras.metrics.Mean(name='similarity_loss')],
                                  sparse_prediction_metrics=[
                                      keras.metrics.SparseCategoricalAccuracy(name='accuracy'),
                                      keras.metrics.SparseTopKCategoricalAccuracy(name='top_5_accuracy', k=5),
                                  ],
                                  dense_prediction_metrics=[
                                      keras.metrics.AUC(name='auc', num_thresholds=200, curve='PR'),
                                      keras.metrics.Precision(name='precision_0.5', top_k=1, thresholds=0.5),
                                      keras.metrics.Recall(name='recall_0.5', top_k=1, thresholds=0.5)
                                  ])
    train_summary_writer = tf.summary.create_file_writer(os.path.join(log_dir, 'train'))
    val_summary_writer = tf.summary.create_file_writer(os.path.join(log_dir, 'val'))
    with resources.open_text('tensorflow_sandbox.data_pipeline', 'imagenet1000_clsidx_to_labels.json') as f:
        label_map = json.load(f)

    label_map = {int(key): value for key, value in label_map.items()}

    logger.info('Training the model')
    import sys
    np.set_printoptions(threshold=sys.maxsize)
    for epoch in range(current_epoch, config.epochs):
        for i, (features, labels) in enumerate(train_dataset):
            current_step = current_step_in_epoch + i + epoch * steps_per_epoch
            loss, predictions = train_step(features, labels, model, loss_object, optimizer, config.train_decoder)

            train_metrics.update_state(labels['label'], predictions['class_logits'], loss)
            if i % config.log_steps == 0:
                train_metrics.log_metrics(current_step)
            if i % config.summary_frequency == 0:
                with train_summary_writer.as_default():
                    train_metrics.summarize_metrics(current_step)

            if i % config.validation_frequency == 0:
                for val_features, val_labels in val_dataset.take(config.validation_steps):
                    val_loss, val_predictions = val_step(val_features, val_labels, model, loss_object,
                                                         config.train_decoder)
                    val_metrics.update_state(val_labels['label'], val_predictions['class_logits'], val_loss)

                with val_summary_writer.as_default():
                    val_metrics.summarize_metrics(current_step)
                    label = label_map[int(labels['label'][0])]
                    prediction = label_map[int(tf.argmax(predictions['class_logits'], axis=1)[0])]
                    summary_image = convert_to_image_space(features['image'][0], config)
                    tf.summary.image(f'classifications/P: {prediction}, T: {label}',
                                     tf.expand_dims(summary_image, axis=0), step=current_step)

                    if isinstance(model, AutoEncoder):
                        summary_decoded_image = convert_to_image_space(predictions['decoded_output'][0], config)
                        tf.summary.image(f'decoded_output', tf.expand_dims(summary_decoded_image, axis=0),
                                         step=current_step)

                val_metrics.log_metrics(current_step, warn=True)
                val_metrics.reset_states()
            if i % config.checkpoint_frequency == 0:
                model.save_weights(os.path.join(log_dir, f'checkpoint_{current_step}.h5'))
            if current_step_in_epoch + i == steps_per_epoch:
                current_step_in_epoch = 0
                break

            train_metrics.reset_states()

        logger.info(f'Finished epoch {epoch + 1}')
        logger.warning(f'Exporting savedmodel...')
        tf.saved_model.save(model, os.path.join(log_dir, 'exports', f'epoch_{epoch + 1}'), signatures=model.infer)

    model.summary()


def convert_to_image_space(normalized_image, config):
    centered_image = normalized_image * config.sdev_pixel
    image = centered_image + config.mean_pixel
    image = tf.cast(image, tf.uint8)
    return image


def load_checkpoint_if_relevant(model: keras.Model, log_dir: str, dataset: tf.data.Dataset, loss_object: LossContainer,
                                optimizer: keras.optimizers.Optimizer, steps_per_epoch: int, max_checkpoints: int,
                                train_decoder: bool) \
        -> (keras.Model, int):
    logs = os.listdir(log_dir)
    checkpoints = [file for file in logs if file.startswith('checkpoint')]
    if len(checkpoints) >= max_checkpoints:
        checkpoints = sorted(checkpoints, key=lambda path: int(path.split('_')[-1].split('.')[0]))
        checkpoints_to_delete = checkpoints[:len(checkpoints) - max_checkpoints]
        for checkpoint in checkpoints_to_delete:
            os.remove(os.path.join(log_dir, checkpoint))

    logs = os.listdir(log_dir)
    checkpoints = [file for file in logs if file.startswith('checkpoint')]

    if checkpoints:
        steps = np.array([int(checkpoint.split('_')[-1].split('.')[0]) for checkpoint in checkpoints])
        checkpoint = checkpoints[int(np.argmax(steps))]
        logger.warning(f'Loading from checkpoint {os.path.join(log_dir, checkpoint)}...')
        # Run batch to initialize model
        for f, l in dataset.take(1):
            train_step(f, l, model, loss_object, optimizer, train_decoder)
        model.load_weights(os.path.join(log_dir, checkpoint))
        current_epoch = int(np.floor(np.max(steps) / steps_per_epoch))
        current_step_in_epoch = int(np.max(steps) - current_epoch * steps_per_epoch)
        return model, current_epoch, current_step_in_epoch
    else:
        return model, 0, 0


@tf.function
def train_step(features, labels, model, loss_object, optimizer, train_decoder):
    with tf.GradientTape() as tape:
        predictions = model(features)
        if isinstance(model, AutoEncoder) and train_decoder:
            consistency_loss = loss_object.autoencoder_loss(features['image'], predictions['decoded_output'])
        else:
            consistency_loss = 0
        category_loss = loss_object.class_loss(labels['label'], predictions['class_logits'])
        total_loss = category_loss + consistency_loss
    gradients = tape.gradient(total_loss, model.trainable_variables)
    optimizer.apply_gradients(zip(gradients, model.trainable_variables))
    return {'category_loss': category_loss, 'consistency_loss': consistency_loss}, predictions


@tf.function
def val_step(features, labels, model, loss_object, train_decoder):
    predictions = model(features)
    category_loss = loss_object.class_loss(labels['label'], predictions['class_logits'])
    if isinstance(model, AutoEncoder) and train_decoder:
        consistency_loss = loss_object.autoencoder_loss(features['image'], predictions['decoded_output'])
    else:
        consistency_loss = 0
    return {'category_loss': category_loss, 'consistency_loss': consistency_loss}, predictions
