import json
import logging
from typing import Dict, List, Tuple

import click
import plotly.graph_objects as go
import numpy as np

logger = logging.getLogger(__name__)


@click.command('summarize', help='Process a metrics file into a model summary.')
@click.argument('metrics-path', type=click.Path(exists=True), nargs=-1, required=True)
def summarize_cli(metrics_path: Tuple):
    fig = go.Figure()
    fig.update_xaxes(range=[0, .045])
    fig.update_yaxes(range=[0, 1])
    fig.update_layout(
        title='PR Curves',
        xaxis_title='Recall',
        yaxis_title='Precision',
        autosize=True
    )
    for i, path in enumerate(metrics_path):
        fig = process_metrics(path, fig, i)
    fig.show()


def process_metrics(metrics_path: str, fig: go.Figure, count: int) -> go.Figure:
    model_name = f'{count}:_{metrics_path.split("/")[-4]}_{metrics_path.split("/")[-2]}'
    with open(metrics_path, 'r') as f:
        data = json.load(f)
    data = cast_data(data)
    ap = []
    for (category, p), (_, r) in zip(data['category_precision'].items(), data['category_recall'].items()):
        ap.append(average_precision(*trim_pr(p, r)))
    map_score = np.mean(ap)

    printable_metrics = f'{count}: Accuracy: {data["accuracy"]:.4f}    Top 5 Acc: {data["top_5_accuracy"]:.4f}    ' \
                        f'AUC: {data["auc"]:.4f}    Approx. mAP: {map_score:.4f}'

    recall = data['recall']
    precision = data['precision']
    precision, recall = trim_pr(precision, recall)

    fig.add_trace(go.Scatter(
        x=recall,
        y=precision,
        hovertext=[f'Thresh: {thresh:.4f}' for thresh in data['thresholds']],
        name=display_nicely(model_name)
    ))
    annotations = fig.layout.annotations + (
        go.layout.Annotation(
            x=0.8,
            y=0.8 - 0.025 * count,
            text=printable_metrics,
            showarrow=False,
            xref='paper',
            yref='paper'
        ),
    )
    fig.update_layout(
        annotations=[ann for ann in annotations],
    )
    return fig


def display_nicely(phrase: str) -> str:
    phrase = phrase.replace('_', ' ')
    phrase = ' '.join([word.capitalize() for word in phrase.split(' ')])

    return phrase


def trim_pr(precision: List[float], recall: List[float]):
    for i, r in enumerate(recall):
        if r == 0:
            if i > 0:
                return precision[:i], recall[:i]
            else:
                return [precision[0]], [recall[0]]


def precision_at_recall(precision: List[float], recall: List[float], target: float) -> (float, float):
    for i, (p, r) in enumerate(zip(precision, recall)):
        if r <= target:
            return p


def average_precision(precision: List[float], recall: List[float]) -> float:
    """
    Computes AP = SUM_i[P_i * (R_i - R_{i-1})]

    Args:
        precision: List of precision values in ascending order.
        recall: List of recall values in descending order.

    Returns:
        Average precision

    """
    if recall[-1] > recall[0]:
        raise ValueError('Recall must be in descending order.')
    old_r = 0
    ap = 0
    precision.reverse()
    recall.reverse()
    for p, r in zip(precision, recall):
        ap += p * (r - old_r)
        old_r = r
    return ap


def cast_data(data: Dict) -> Dict:
    for key, value in data.items():
        if isinstance(value, list):
            data[key] = [float(v) for v in value]
        elif isinstance(value, str):
            data[key] = float(value)
        else:
            data[key] = cast_data(value)
    return data
