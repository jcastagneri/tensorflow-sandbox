import os
import json
from importlib import resources

import click
import matplotlib.pyplot as plt
import numpy as np

from tensorflow_sandbox.data_pipeline.load_imagenet import build_dataset
from tensorflow_sandbox.config import Config


@click.command('visualize-imagenet', help='Visualize images from imagenet2012')
@click.option('--data-dir', help='Location of TF datasets (assumes imagenet2012)', type=click.Path(exists=True),
              default=os.path.join(os.path.expanduser('~'), 'tensorflow-datasets'))
@click.option('--num-to-print', help='Number of images to print', default=4, type=int)
def visualize_imagenet_cli(data_dir: str, num_to_print: int):
    train_set, _ = build_dataset(data_dir)
    batch = train_set.take(1)
    images_for_display = []
    for f, l in batch:
        images = f['image']
        labels = [int(label) for label in l['label']]
        images_for_display = [images[i, ...] for i in range(num_to_print)]

    with resources.open_text('tensorflow_sandbox.data_pipeline', 'imagenet1000_clsidx_to_labels.json') as f:
        label_map = json.load(f)

    label_map = {int(key): value for key, value in label_map.items()}

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])

    for i in range(num_to_print):
        ax1 = fig.add_subplot(np.ceil(num_to_print / 2), 2, i + 1)
        ax1.imshow(process_image(images_for_display[i]))
        ax1.axis('off')
        ax1.set_title(f'{label_map[labels[i]]}')
    plt.show()


def process_image(image):
    image += Config.mean_pixel
    image = np.clip(image, 0, 255).astype(np.uint8)
    return image
