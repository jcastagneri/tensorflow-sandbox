import os
import logging

import click
import numpy as np
import plotly.graph_objects as go
from tqdm import tqdm

from tensorflow_sandbox.config import config_factory
from tensorflow_sandbox.data_pipeline.load_imagenet import build_dataset

logger = logging.getLogger(__name__)


@click.command('compute-normalization')
@click.option('--data-dir', help='Location of TF datasets (assumes imagenet2012)', type=click.Path(exists=True),
              default=os.path.join(os.path.expanduser('~'), 'data', 'tensorflow-datasets'))
def compute_normalization_cli(data_dir: str):
    config = config_factory(experiment=True)
    dataset, _, dataset_info = build_dataset(data_dir, config, experiment=True)
    batch_aves = []
    batch_sdevs = []
    for f, l in tqdm(dataset.take(1000)):
        image = f['image']
        batch = [image[i, ...] for i in range(config.batch_size)]
        image_aves = []
        image_sdevs = []
        for image in batch:
            image = image.numpy()
            channels = [image[..., i] for i in range(3)]
            image_aves.append([np.average(np.average(channel, axis=0)) for channel in channels])
            image_sdevs.append([np.average(np.std(channel, axis=0)) for channel in channels])
        image_aves = np.array(image_aves)
        batch_aves.append(np.average(image_aves, axis=0))
        image_sdevs = np.array(image_sdevs)
        batch_sdevs.append(np.average(image_sdevs, axis=0))
    batch_aves = np.array(batch_aves)
    batch_sdevs = np.array(batch_sdevs)
    mean_pixel = np.average(batch_aves, axis=0)
    mean_sdev = np.average(batch_sdevs, axis=0)
    logger.info(f'Mean pixel: {mean_pixel}')
    logger.info(f'Mean sdev: {mean_sdev}')


@click.command('histogram-pixels')
@click.option('--data-dir', help='Location of TF datasets (assumes imagenet2012)', type=click.Path(exists=True),
              default=os.path.join(os.path.expanduser('~'), 'data', 'tensorflow-datasets'))
def histogram_pixels_cli(data_dir: str):
    config = config_factory(experiment=True)
    dataset, _, dataset_info = build_dataset(data_dir, config, experiment=True)
    pixels = []
    for f, l in tqdm(dataset.take(100)):
        image = f['image']
        batch = [image[i, ...] for i in range(config.batch_size)]
        for image in batch:
            image = image.numpy()
            flat_image = list(image.flatten())
            pixels += [np.random.choice(flat_image)]

    fig = go.Figure(data=[go.Histogram(x=pixels, histnorm='probability')])
    fig.update_xaxes(range=[-5, 5])
    fig.update_layout(
        title='Pixel Intensity Histogram (Post Normalization)',
        xaxis_title='Pixel Intensity',
        yaxis_title='Probability',
        autosize=True
    )
    fig.show()


