import logging
import os
import time

import click
import numpy as np
import tensorflow as tf
from tensorflow import keras

from tensorflow_sandbox.config import config_factory
from tensorflow_sandbox.data_pipeline.load_imagenet import build_inference_testset
from tensorflow_sandbox.utils.metrics import MetricContainer
from tensorflow_sandbox.utils.metrics.containers import PrecisionContainer, RecallContainer

logger = logging.getLogger(__name__)


@click.command('test', help='Run test set against model and compute metrics.')
@click.argument('savedmodel-path', type=click.Path(exists=True))
@click.option('--data-dir', help='Location of TF datasets (assumes imagenet2012)', type=click.Path(exists=True),
              default=os.path.join(os.path.expanduser('~'), 'data', 'tensorflow-datasets'))
@click.option('--num-thresholds', help='Number of thresholds to use for PR', type=int,
              default=500, show_default=True)
@click.option('--num-test-steps', help='Number of steps to run testing', type=int)
def test_cli(savedmodel_path: str, data_dir: str, num_thresholds: int, num_test_steps: int):
    thresholds = [float(x) for x in np.arange(0, 1.0, 1 / (num_thresholds - 1))]
    config = config_factory(experiment=False)
    val_dataset = build_inference_testset(data_dir, config)
    logger.info(f'Loading savedmodel at {savedmodel_path}...')
    model = tf.saved_model.load(savedmodel_path)
    signature = model.signatures['serving_default']

    metrics = MetricContainer(num_categories=1000, sparse_prediction_metrics=[
        keras.metrics.SparseCategoricalAccuracy(name='accuracy'),
        keras.metrics.SparseTopKCategoricalAccuracy(name='top_5_accuracy', k=5)
    ], dense_prediction_metrics=[
        PrecisionContainer(name='category_precision', thresholds=thresholds, top_k=1, num_categories=75),
        RecallContainer(name='category_recall', thresholds=thresholds, top_k=1, num_categories=75),
        keras.metrics.Precision(name='precision', thresholds=thresholds, top_k=1),
        keras.metrics.Recall(name='recall', thresholds=thresholds, top_k=1),
        keras.metrics.AUC(name='auc', num_thresholds=len(thresholds), curve='PR')
    ])
    durations = []
    for i, (f, l) in enumerate(val_dataset):
        start_time = time.time()
        signature(f['image'])['output_0'].numpy()
        durations.append(time.time() - start_time)
        if i == num_test_steps:
            break

    duration_per_image = [duration / config.batch_size for duration in durations]
    fps = [1 / duration for duration in duration_per_image]
    logger.info(f'Average FPS: {np.average(fps):.4f}, STDev: {np.std(fps):.4f}')

    for i, (f, l) in enumerate(val_dataset):
        preds = signature(f['image'])['output_0'].numpy()
        metrics.update_state(l['label'], preds)
        metrics.log_metrics(i)
        if num_test_steps and i == num_test_steps:
            break

    metrics.write_out_metrics(os.path.join(savedmodel_path, 'test_metrics.json'), thresholds)
