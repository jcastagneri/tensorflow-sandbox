import json
import logging
from abc import ABC, abstractmethod
from typing import List, Optional

import numpy as np
import tensorflow as tf

logger = logging.getLogger(__name__)


class ABCMetricContainer(ABC):
    def __init__(self, num_categories: int):
        self.num_categories = num_categories

    def __call__(self, *args, **kwargs):
        self.update_state(*args, **kwargs)

    @property
    @abstractmethod
    def metrics(self):
        pass

    def update_state(self, ground_truth, predictions, *args, **kwargs):
        """
        Update the metrics contained in the object

        Args:
            ground_truth: (batch, labels) one hot encoded ground truth or (batch, ) dense labels
            predictions: (batch, labels) predictions

        """
        for metric in self.metrics:
            metric(ground_truth, predictions)

    def reset_states(self):
        for metric in self.metrics:
            metric.reset_states()

    def result(self):
        values = [metric.result() for metric in self.metrics]
        return values


class MetricContainer(ABCMetricContainer):
    def __init__(self, num_categories: int, loss_metrics: Optional[List] = None,
                 dense_prediction_metrics: Optional[List] = None, sparse_prediction_metrics: Optional[List] = None):
        super(MetricContainer, self).__init__(num_categories)
        self._loss_metrics = loss_metrics if loss_metrics else []
        self._dense_prediction_metrics = dense_prediction_metrics if dense_prediction_metrics else []
        self._sparse_prediction_metrics = sparse_prediction_metrics if sparse_prediction_metrics else []

    @property
    def metrics(self):
        return self._loss_metrics + self._sparse_prediction_metrics + self._dense_prediction_metrics

    def update_state(self, ground_truth, predictions, *args, **kwargs):
        if args or kwargs:
            losses = args[0] if not kwargs.get('loss') else kwargs.get('loss')
        else:
            losses = {}
        for metric in self._dense_prediction_metrics:
            metric(tf.one_hot(ground_truth, depth=tf.shape(predictions)[1]), predictions)
        for metric in self._sparse_prediction_metrics:
            metric(ground_truth, predictions)
        for metric, loss in zip(self._loss_metrics, losses.values()):
            metric(loss)

    def summarize_metrics(self, step: int):
        for metric in self.metrics:
            tf.summary.scalar(metric.name, metric.result(), step=step)

    def log_metrics(self, step: int, warn: bool = False):
        log = f'step: {step:10}    '
        for metric in self.metrics:
            result = metric.result()
            if len(tf.shape(result)) >= 1:
                result = np.mean(result)
            log += f'{metric.name}: {result:.6f}    '
        if warn:
            logger.warning(log)
        else:
            logger.info(log)

    def write_out_metrics(self, filepath: str, thresholds: List[float] = None):
        data = {}
        for metric in self.metrics:
            if len(tf.shape(metric.result())) == 0:
                data[metric.name] = str(metric.result().numpy())
            elif len(tf.shape(metric.result())) == 1:
                cast_data = [str(res.numpy()) for res in metric.result()]
                data[metric.name] = cast_data
            elif len(tf.shape(metric.result())) == 2:
                cast_data = {i: [str(res) for res in class_result.numpy()]
                             for i, class_result in enumerate(metric.result())}
                data[metric.name] = cast_data
            else:
                raise NotImplementedError(f'Cannot handle metric of shape {tf.shape(metric.result())}')
        if thresholds:
            data['thresholds'] = [str(threshold) for threshold in thresholds]

        with open(filepath, 'w') as f:
            json.dump(data, f, indent=2)


