from typing import List, Union

from tensorflow import keras

from tensorflow_sandbox.utils.metrics import ABCMetricContainer


class PrecisionContainer(ABCMetricContainer):
    def __init__(self, name: str, thresholds: Union[List[float], float], top_k: int, num_categories: int):
        super(PrecisionContainer, self).__init__(num_categories=num_categories)
        self.name = name
        self.thresholds = thresholds
        self.top_k = top_k
        self._metrics = None

    @property
    def metrics(self):
        if not self._metrics:
            metrics: List[keras.metrics] = [keras.metrics.Precision(thresholds=self.thresholds, top_k=self.top_k,
                                                                    class_id=i, name=f'{self.name}_{i}')
                                            for i in range(self.num_categories)]
            self._metrics = metrics
        else:
            metrics = self._metrics
        return metrics


class RecallContainer(ABCMetricContainer):
    def __init__(self, name: str, thresholds: Union[List[float], float], top_k: int, num_categories: int):
        super(RecallContainer, self).__init__(num_categories=num_categories)
        self.name = name
        self.thresholds = thresholds
        self.top_k = top_k
        self._metrics = None

    @property
    def metrics(self):
        if not self._metrics:
            metrics: List[keras.metrics] = [keras.metrics.Recall(thresholds=self.thresholds, top_k=self.top_k,
                                                                 class_id=i, name=f'{self.name}_{i}')
                                            for i in range(self.num_categories)]
            self._metrics = metrics
        else:
            metrics = self._metrics
        return metrics


