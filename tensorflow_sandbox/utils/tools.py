from tensorflow_sandbox.config import Config
from tensorflow_sandbox.models.resnet import ResNet
from tensorflow_sandbox.models.autoencoder import AutoEncoder


def build_model(model: str, config: Config):
    if model == 'resnet':
        return ResNet(config)
    elif model == 'autoencoder':
        return AutoEncoder(config)
    else:
        raise NotImplementedError(f'{model} does not exist.')