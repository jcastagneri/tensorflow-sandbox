from tensorflow import keras


class ActivatedBatchNormedConvolution(keras.layers.Layer):
    def __init__(self, kernels: int, stride: int, filters: int, weight_decay: float, transpose: bool = False):
        super(ActivatedBatchNormedConvolution, self).__init__()
        if transpose:
            self.conv_layer = keras.layers.Conv2DTranspose(filters=filters, kernel_size=kernels, strides=stride,
                                                           padding='same',
                                                           kernel_regularizer=keras.regularizers.l2(weight_decay))
        else:
            self.conv_layer = keras.layers.Conv2D(filters=filters, kernel_size=kernels, strides=stride, padding='same',
                                                  kernel_regularizer=keras.regularizers.l2(weight_decay))
        self.batch_norm = keras.layers.BatchNormalization(axis=3)
        self.activation = keras.layers.ReLU()

    def call(self, inputs):
        x = self.conv_layer(inputs)
        x = self.batch_norm(x, training=True)
        return self.activation(x)


class ProjectionBlockUnder34(keras.layers.Layer):
    def __init__(self, filters: int, weight_decay: float, transpose: bool = False):
        super(ProjectionBlockUnder34, self).__init__()
        self.projection_mapping = ActivatedBatchNormedConvolution(kernels=1, stride=2, filters=filters,
                                                                  weight_decay=weight_decay, transpose=transpose)
        self.first_convolution = ActivatedBatchNormedConvolution(kernels=3, stride=2, filters=filters,
                                                                 weight_decay=weight_decay, transpose=transpose)
        self.second_convolution = ActivatedBatchNormedConvolution(kernels=3, stride=1, filters=filters,
                                                                  weight_decay=weight_decay, transpose=transpose)
        self.shortcut = keras.layers.Add()

    def call(self, inputs):
        projected_inputs = self.projection_mapping(inputs)
        x = self.first_convolution(inputs)
        x = self.second_convolution(x)
        return self.shortcut([projected_inputs, x])


class IdentityBlockUnder34(keras.layers.Layer):
    def __init__(self, filters: int, weight_decay: float, transpose: bool = False):
        super(IdentityBlockUnder34, self).__init__()
        self.first_convolution = ActivatedBatchNormedConvolution(kernels=3, stride=1, filters=filters,
                                                                 weight_decay=weight_decay, transpose=transpose)
        self.second_convolution = ActivatedBatchNormedConvolution(kernels=3, stride=1, filters=filters,
                                                                  weight_decay=weight_decay, transpose=transpose)
        self.shortcut = keras.layers.Add()

    def call(self, inputs):
        x = self.first_convolution(inputs)
        x = self.second_convolution(x)
        return self.shortcut([inputs, x])


class FirstBlock(keras.layers.Layer):
    def __init__(self, config, transpose: bool = False):
        super(FirstBlock, self).__init__()
        if config.num_layers <= 34:
            self.group_one = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_two = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_three = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
        else:
            raise NotImplementedError()

    def call(self, inputs):
        x = self.group_one(inputs)
        x = self.group_two(x)
        return self.group_three(x)


class SecondBlock(keras.layers.Layer):
    def __init__(self, config, transpose: bool = False):
        super(SecondBlock, self).__init__()
        if config.num_layers <= 34:
            self.group_one = ProjectionBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_two = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_three = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_four = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
        else:
            raise NotImplementedError()

    def call(self, inputs):
        x = self.group_one(inputs)
        x = self.group_two(x)
        x = self.group_three(x)
        return self.group_four(x)


class ThirdBlock(keras.layers.Layer):
    def __init__(self, config, transpose: bool = False):
        super(ThirdBlock, self).__init__()
        if config.num_layers <= 34:
            self.group_one = ProjectionBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_two = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_three = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_four = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_five = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_six = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
        else:
            raise NotImplementedError()

    def call(self, inputs):
        x = self.group_one(inputs)
        x = self.group_two(x)
        x = self.group_three(x)
        x = self.group_four(x)
        x = self.group_five(x)
        return self.group_six(x)


class FourthBlock(keras.layers.Layer):
    def __init__(self, config, transpose: bool = False):
        super(FourthBlock, self).__init__()
        if config.num_layers <= 34:
            self.group_one = ProjectionBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_two = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
            self.group_three = IdentityBlockUnder34(filters=64, weight_decay=config.weight_decay, transpose=transpose)
        else:
            raise NotImplementedError()

    def call(self, inputs):
        x = self.group_one(inputs)
        x = self.group_two(x)
        return self.group_three(x)
