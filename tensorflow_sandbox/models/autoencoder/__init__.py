import tensorflow as tf
from tensorflow import keras

from tensorflow_sandbox.models.autoencoder.layers import FirstBlock, SecondBlock, ThirdBlock, FourthBlock


class AutoEncoder(keras.Model):
    def __init__(self, config):
        super(AutoEncoder, self).__init__()
        # TODO don't build decoder for inference
        self.config = config
        self.input_conv = keras.layers.Conv2D(filters=64, kernel_size=7, strides=2, padding='same',
                                              input_shape=(config.image_height, config.image_width, 3),
                                              kernel_regularizer=keras.regularizers.l2(config.weight_decay))
        self.max_pool_layer = keras.layers.MaxPool2D(pool_size=3, strides=2, padding='same')

        self.encode_first_block = FirstBlock(config)
        self.encode_second_block = SecondBlock(config)
        self.encode_third_block = ThirdBlock(config)
        self.encode_fourth_block = FourthBlock(config)

        self.global_average_pooling = keras.layers.AveragePooling2D(pool_size=1, strides=7)
        self.flatten = keras.layers.Flatten()
        self.final_dense = keras.layers.Dense(units=1000, activation='softmax',
                                              kernel_regularizer=keras.regularizers.l2(config.weight_decay))

        self.decode_first_block = FirstBlock(config, transpose=True)
        self.decode_second_block = SecondBlock(config, transpose=True)
        self.decode_third_block = ThirdBlock(config, transpose=True)
        self.decode_fourth_block = FourthBlock(config, transpose=True)

        self.penultimate_deconvolution = \
            keras.layers.Conv2DTranspose(filters=32, kernel_size=3, strides=2, padding='same',
                                         kernel_regularizer=keras.regularizers.l2(config.weight_decay))
        self.final_deconvolution = \
            keras.layers.Conv2DTranspose(filters=3, kernel_size=7, strides=2, padding='same',
                                         kernel_regularizer=keras.regularizers.l2(config.weight_decay))

    def call(self, features):
        """Currently implements a ResNet based autoencoder

        Args:
            features: Dict with keys (image) and value a (?, 224, 224, 3) image batch

        Returns:
            1000-D probability distribution

        """
        x = features['image']  # 224 x 224 x 3
        x = self.input_conv(x)  # 112 x 112 x 64
        x = self.max_pool_layer(x)  # 56 x 56 x 64

        # Convolution blocks
        x = self.encode_first_block(x)
        x = self.encode_second_block(x)  # 28 x 28 x 64
        x = self.encode_third_block(x)  # 14 x 14 x 64
        x = self.encode_fourth_block(x)  # 7 x 7 x 64

        # Average Pool
        class_head = self.global_average_pooling(x)  # 1 x 1 x 64

        # Logits Layer
        class_head = self.flatten(class_head)
        class_head = self.final_dense(class_head)

        # Deconvolution blocks
        x = self.decode_first_block(x)  # 7 x 7 x 64
        x = self.decode_second_block(x)  # 14 x 14 x 64
        x = self.decode_third_block(x)  # 28 x 28 x 64
        x = self.decode_fourth_block(x)  # 56 x 56 x 64

        x = self.penultimate_deconvolution(x)  # 224 x 224 x 32
        decoded_output = self.final_deconvolution(x)  # 224 x 224 x 3

        return {
            'class_logits': class_head,
            'decoded_output': decoded_output
        }

    def run_encoder(self, features):
        x = features['image']  # 224 x 224 x 3
        x = self.input_conv(x)  # 112 x 112 x 64
        x = self.max_pool_layer(x)  # 56 x 56 x 64

        # Convolution blocks
        x = self.encode_first_block(x)
        x = self.encode_second_block(x)  # 28 x 28 x 64
        x = self.encode_third_block(x)  # 14 x 14 x 64
        x = self.encode_fourth_block(x)  # 7 x 7 x 64

        # Average Pool
        class_head = self.global_average_pooling(x)  # 1 x 1 x 64

        # Logits Layer
        class_head = self.flatten(class_head)
        class_head = self.final_dense(class_head)

        return {
            'class_logits': class_head,
        }

    @tf.function(input_signature=[tf.TensorSpec([None, None, None, 3], tf.uint8)])
    def infer(self, image_batch):
        resized_batch = tf.image.resize_with_crop_or_pad(image_batch, target_height=self.config.image_height,
                                                         target_width=self.config.image_width)
        cast_images = tf.cast(resized_batch, dtype=tf.float32)
        processed_images = tf.divide(cast_images - self.config.mean_pixel, self.config.sdev_pixel)
        return self.run_encoder({'image': processed_images})['class_logits']
