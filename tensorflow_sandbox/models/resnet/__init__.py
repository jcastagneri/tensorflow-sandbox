import tensorflow as tf
from tensorflow import keras

from tensorflow_sandbox.models.resnet.layers import FirstBlock, SecondBlock, ThirdBlock, FourthBlock


class ResNet(keras.Model):
    def __init__(self, config):
        super(ResNet, self).__init__()
        self.config = config
        self.input_conv = keras.layers.Conv2D(filters=64, kernel_size=7, strides=2, padding='same',
                                              input_shape=(config.image_height, config.image_width, 3),
                                              kernel_regularizer=keras.regularizers.l2(config.weight_decay))
        self.max_pool_layer = keras.layers.MaxPool2D(pool_size=3, strides=2, padding='same')

        self.first_block = FirstBlock(config)
        self.second_block = SecondBlock(config)
        self.third_block = ThirdBlock(config)
        self.fourth_block = FourthBlock(config)

        self.global_average_pooling = keras.layers.AveragePooling2D(pool_size=1, strides=7)
        self.flatten = keras.layers.Flatten()
        self.final_dense = keras.layers.Dense(units=1000, activation='softmax',
                                              kernel_regularizer=keras.regularizers.l2(config.weight_decay))

    def call(self, features):
        """Currently implements a 34 layer ResNet

        Args:
            features: Dict with keys (image) and value a (?, 224, 224, 3) image batch

        Returns:
            1000-D probability distribution

        """
        x = features['image']  # 224 x 224
        x = self.input_conv(x)  # 112 x 112 x 64
        x = self.max_pool_layer(x)  # 56 x 56 x 64

        # Convolution blocks
        x = self.first_block(x)
        x = self.second_block(x)  # 28 x 28 x 128
        x = self.third_block(x)  # 14 x 14 x 256
        x = self.fourth_block(x)  # 7 x 7 x 512

        # Average Pool
        x = self.global_average_pooling(x)  # 1 x 1 x 512

        # Logits Layer
        x = self.flatten(x)
        x = self.final_dense(x)
        return {'class_logits': x}

    @tf.function(input_signature=[tf.TensorSpec([None, None, None, 3], tf.uint8)])
    def infer(self, image_batch):
        resized_batch = tf.image.resize_with_crop_or_pad(image_batch, target_height=self.config.image_height,
                                                         target_width=self.config.image_width)
        cast_images = tf.cast(resized_batch, dtype=tf.float32)
        processed_images = tf.divide(cast_images - self.config.mean_pixel, self.config.sdev_pixel)
        return self.call({'image': processed_images})['class_logits']
