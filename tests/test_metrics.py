import numpy as np
import tensorflow as tf
from tensorflow import keras

from tensorflow_sandbox.bin.summarize_model import precision_at_recall, trim_pr, average_precision
from tensorflow_sandbox.utils.metrics.containers import RecallContainer, PrecisionContainer


def test_precision():
    p = keras.metrics.Precision(top_k=1)
    r = keras.metrics.Recall(top_k=1)
    # (batch_size, classes) = (5, 3) for these examples
    truth = np.array([[0, 1, 0],
                      [1, 0, 0],
                      [0, 0, 1],
                      [0, 1, 0],
                      [0, 0, 1]])
    pred = np.array([[.2, .6, .2],
                     [.1, .5, .4],
                     [.7, .1, .2],
                     [.4, .5, .1],
                     [.1, .1, .8]])
    # Total summation method
    # Total TP: 3   FP: 2   FN: 2
    p.update_state(truth, pred)
    r.update_state(truth, pred)
    assert np.isclose(p.result().numpy(), 3 / (3 + 2))  # 0.6
    assert np.isclose(r.result().numpy(), 3 / (3 + 2))  # 0.6

    # PR per category calculated over all batches and then averaged
    # Label 0: TP: 0   FP: 1   FN: 1   P: 0 / 1   R: 0 / 1
    # Label 1: TP: 2   FP: 1   FN: 0   P: 2 / 3   R: 2 / 2
    # Label 2: TP: 1   FP: 0   FN: 1   P: 1 / 1   R: 1 / 2
    # Ave P = (1 / 3) * (5 / 3) = 5 / 9
    # Ave R = (1 / 3) * (3 / 2) = 1 / 2
    # To implement this with TF, you must use the class_id flag for each class_id, top_k to negate non-choice
    # predictions, and threshold to give P at a threshold.
    p0 = keras.metrics.Precision(top_k=1, class_id=0)
    p0.update_state(truth, pred)
    p1 = keras.metrics.Precision(top_k=1, class_id=1, thresholds=0.4)
    p1.update_state(truth, pred)
    p2 = keras.metrics.Precision(top_k=1, class_id=2)
    p2.update_state(truth, pred)
    mP = tf.reduce_mean([p0.result(), p1.result(), p2.result()])
    assert np.isclose(p0.result().numpy(), 0)
    assert np.isclose(p1.result().numpy(), 2 / 3)
    assert np.isclose(p2.result().numpy(), 1)
    assert np.isclose(mP, 5 / 9)


def test_cat_pr():
    """
    Predictions @0.1
    [F, T, F]
    [F, T, F]
    [T, F, F]
    [F, T, F]
    [F, F, T]

    GT
    [F, T, F]
    [T, F, F]
    [F, F, T]
    [F, T, F]
    [F, F, T]

    [TN, TP, TN]
    [FN, FP, TN]
    [FP, TN, FN]
    [TN, TP, TN]
    [TN, TN, TP]

    P1: 0    , R1: 0
    P2: 2 / 3, R2: 2 / 2
    P3: 1 / 1, R3: 1 / 2

    """
    thresholds = [float(x) for x in np.arange(0, 1.05, 0.1)]
    truth = np.array([1, 0, 2, 1, 2])
    pred = np.array([[.2, .6, .2],
                     [.1, .5, .4],
                     [.7, .1, .2],
                     [.4, .5, .1],
                     [.1, .1, .8]])
    one_hot_truth = tf.one_hot(truth, depth=tf.shape(pred)[1])
    precision_container = PrecisionContainer('precision', thresholds, top_k=1, num_categories=3)
    recall_container = RecallContainer('recall', thresholds, top_k=1, num_categories=3)
    precision_container.update_state(one_hot_truth, pred)
    recall_container.update_state(one_hot_truth, pred)

    precisions = [p.numpy() for p in precision_container.result()]
    recalls = [r.numpy() for r in recall_container.result()]

    assert np.allclose(precisions[1], np.array([2/3, 2/3, 2/3, 2/3, 2/3, 1, 0, 0, 0, 0, 0]))
    assert np.allclose(recalls[1], np.array([1, 1, 1, 1, 1, 1/2, 0, 0, 0, 0, 0]))


def test_average_precision():
    precision = [0.2, 0.4, 0.6]
    recall = [0.8, 0.6, 0.4]
    ap = average_precision(precision, recall)
    assert ap == 0.6 * 0.4 + 0.4 * 0.2 + 0.2 * 0.2
