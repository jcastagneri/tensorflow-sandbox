# Tensorflow Sandbox
Sandbox for various classification architectures in TF2. Complete with evaluation tools and visualization
to explore new ideas.

## System Deps
- Cuda 10.1
- CuDNN 7
- Python 3.7
- Python Poetry

## Python Deps
Install Python dependencies by running `poetry install` in the root of the repository.

## Data Deps
These nets use Imagenet. Instructions to download the Tensorflow datasets compatible version can be 
found [here](https://www.tensorflow.org/datasets/catalog/imagenet2012).

## The Training CLI
After running `poetry install`, you will have a binary available in your virtualenv called 
`tf-sandbox`. This CLI exposes training, visualization, and evaluation functionality implemented in this
repository. See the helptext of this CLI for more details with `poetry run tf-sandbox --help`.
